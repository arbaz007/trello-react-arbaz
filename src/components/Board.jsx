import styled from "@emotion/styled";
import {
  Box,
  List,
  ListItem,
  Modal,
  Typography,
  Button,
  Input,
  FormLabel,
  LinearProgress,
} from "@mui/material";
import axios from "axios";
import { useEffect, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { AddBox, Edit } from "@mui/icons-material";
import CreateList from "./CreateList";
import Lists from "./Lists";
import AddComp from "./AddComp";
import { createItem, getItem } from "../getItem";
const Board = () => {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const [lists, setLists] = useState([]);
  const [input, setInput] = useState("");
  const { id } = useParams();
  const [loader, setLoader] = useState(true);
  const [background, setBackground] = useState({
    image: "",
    color: "",
  });
  useEffect(() => {
    const getBoard = async () => {
      try {
        let data = await axios.get(
          `https://api.trello.com/1/boards/${id}?key=${apiKey}&token=${token}`,
          {
            headers: {
              Accept: "application/json",
            },
          }
        );
        if (!data) {
          throw new Error("error occured while getting a board");
        }
        data = data.data;
        const image =
          data?.prefs?.backgroundImage && data?.prefs?.backgroundImage;
        const color =
          data?.prefs?.backgroundColor && data?.prefs?.backgroundColor;
        setBackground({
          image,
          color,
        });
      } catch (err) {
        console.log("error occured: ", err);
      }
    };
    getBoard();
  }, []);
  useEffect(() => {
    const getLists = async () => {
      getItem("boards", id, "lists", setLists);
      setLoader(false);
    };
    getLists();
  }, []);

  const deleteList = async (idList) => {
    try {
      let data = await axios.put(
        `https://api.trello.com/1/lists/${idList}/closed?key=${apiKey}&token=${token}&value=true`
      );
      if (!data) {
        throw new Error("error while deleting lists data");
      }
      setLists(lists.filter((list) => list?.id !== idList));
    } catch (err) {
      console.log("error occured in delete list: ", err.message);
    }
  };

  const addList = async (id) => {
    const list = await createItem(id, "idBoard", input, "lists");
    setLists((prev) => [...prev, list]);
  };

  return (
    <>
      <Box
        sx={{
          height: "100vh",
          width: "100vw",
          background: background.image
            ? `url(${background.image})`
            : background.color,
          backgroundSize: "cover",
          backgroundPosition: "center",
          overflow: "hidden"
        }}
      >
        {loader && (
          <Box sx={{ width: "100%", marginTop: "10rem" }}>
            <LinearProgress />
          </Box>
        )}
        {!loader && (
          <Box
            sx={{
              display: "flex",
              alignItems: "flex-start",
              gap: "1rem",
              overflowX: "scroll",
              paddingTop: "1rem",
              width: "100vw",
              height: "100vh"
            }}
          >
            {lists.map((list) => {
              return (
                <Lists
                  list={list}
                  key={list?.id}
                  idList={list?.id}
                  deleteList={deleteList}
                />
              );
            })}
            <AddComp
              name={"list"}
              addComp={addList}
              parentId={id}
              input={input}
              setInput={setInput}
            />
          </Box>
        )}
      </Box>
    </>
  );
};
export default Board;

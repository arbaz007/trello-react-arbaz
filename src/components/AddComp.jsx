import { Add, Close } from "@mui/icons-material";
import { Button, FormLabel, Input, TextField } from "@mui/material";
import { useState } from "react";

const AddComp = ({ name, addComp, parentId, input, setInput }) => {
  const [showinput, setShowInput] = useState(false);
  let style =
    (name === "list" && {
      backdropFilter: "blur(16px) saturate(180%)",
      backgroundColor: "rgba(255, 255, 255, 0.75)",
      borderRadius: "6px",
      border: "1px solid rgba(209, 213, 219, 0.3)",
      "&:hover": {
        backgroundColor: "rgba(255, 255, 255, 0.75)",
      }
    }) ||
    (name === "card" && {
      margin: "01rem 0",
    }) || (
      name === "Check Item" && {
        fontSize : "0.8rem"
      }
    )
  style = { ...style, color: "#00224D" };
  return (
    <>
      {!showinput && (
        <Button onClick={() => setShowInput(true)} sx={style}>
          Create {name}
          <Add />
        </Button>
      )}
      {showinput && (
        <form
          onSubmit={(e) => {
            e.preventDefault();
            addComp(parentId);
            setShowInput(false);
            setInput("");
          }}
        >
          <Input
            value={input}
            onChange={(e) => setInput(e.target.value)}
          ></Input>
          <Input
            type="submit"
            value={"Add"}
            sx={{ color: "#00224D", borderBottom: "none", "&:hover": { cursor: "pointer" } }}
          />

          <Close
            onClick={() => {
              setShowInput(false);
              setInput("");
            }}
            sx={{ "&:hover": { cursor: "pointer" } }}
          />
        </form>
      )}
    </>
  );
};

export default AddComp;

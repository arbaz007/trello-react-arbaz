import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import CreateBoard from "./CreateBoard";
import { Box } from "@mui/material";

let Boards = () => {
  const apiKey = import.meta.env.VITE_API_KEY;
  const token = import.meta.env.VITE_TOKEN;
  const [allBoards, setAllBoards] = useState([]);
  const navigate = useNavigate();
  useEffect(() => {
    let displayAllBoards = async () => {
      let getAllBoards = await axios.get(
        `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`,
        {
          headers: {
            Accept: "application/json",
          },
        }
      );
      getAllBoards = getAllBoards.data;
      setAllBoards(getAllBoards);
    };
    displayAllBoards();
  }, []);

  return (
    <>
      <Box sx={{ display: "flex", flexWrap: "wrap", gap: "1rem"}}>
        <CreateBoard allBoards={allBoards} setAllBoards={setAllBoards} />
        {allBoards.map((each) => {
          return (
            <Box
              key={each?.id}
              sx={{
                background: each?.prefs?.backgroundImage
                  ? `url(${each?.prefs?.backgroundImage})`
                  : each?.prefs?.backgroundColor,
                backgroundSize: "cover",
                backgroundPosition: "center",
                padding: "2rem",
                width: "150px",
                fontWeight: "bold",
                borderRadius: "0.5rem",
              }}
              onClick={() => navigate(`/board/${each?.id}`)}
            >
              {each.name}
            </Box>
          );
        })}
      </Box>
    </>
  );
};

export default Boards;
